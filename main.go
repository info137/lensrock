package main

import (
	"fmt"
	"lensrock/views"
	"net/http"

	"github.com/gorilla/mux"
)

var (
	homeView    *views.View
	contactView *views.View
)

func home(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content/type", "text/html")

	err := homeView.Template.ExecuteTemplate(w, homeView.Layout, nil)

	if err != nil {
		panic(err)
	}
}

func contact(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Context/type", "text/html")

	err := contactView.Template.ExecuteTemplate(w, contactView.Layout, nil)

	if err != nil {
		panic(err)
	}
}

func main() {
	fmt.Println("Working...")

	homeView = views.NewView("bootstrap", "views/home.gohtml")
	contactView = views.NewView("bootstrap", "views/contact.gohtml")

	r := mux.NewRouter()
	r.HandleFunc("/", home)
	r.HandleFunc("/contact", contact)
	http.ListenAndServe(":3000", r)
}
