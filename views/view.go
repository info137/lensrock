package views

import (
	"html/template"
	"path/filepath"
)

var (
	TemplateExt = ".gohtml"
	LayoutDir   = "views/layouts/"
)

func NewView(layout string, files ...string) *View {
	files = append(files, layoutFiles()...)
	// 	"views/layouts/bootstrap.gohtml",
	// 	"views/layouts/navbar.gohtml",
	// 	"views/layouts/footer.gohtml",
	// )
	t, err := template.ParseFiles(files...)
	if err != nil {
		panic(err)
	}

	return &View{
		Template: t,
		Layout:   layout,
	}
}

type View struct {
	Template *template.Template
	Layout   string
}

func layoutFiles() []string {
	// files, err := filepath.Glob("views/layouts/*.gohtml")
	files, err := filepath.Glob(LayoutDir + "*" + TemplateExt)
	if err != nil {
		panic(err)
	}
	return files
}
